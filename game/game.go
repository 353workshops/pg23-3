package main

import (
	"fmt"
	"log"
)

func main() {
	i := Item{20, 30} // by position, must pass all fields
	fmt.Println("i:", i)

	i = Item{Y: -10} //, X: 30} // by name, order not important, can omit
	fmt.Println("i:", i)

	i = Item{
		X: 30,
		Y: 40,
	}
	fmt.Println("i:", i)
	fmt.Printf(" v: %v\n", i)
	fmt.Printf("+v: %+v\n", i)
	fmt.Printf("#v: %#v\n", i)

	// Use %#v in logging, debug printing
	a, b := 1, "1"
	log.Printf("a=%#v, b=%#v", a, b)

	fmt.Println(NewItem(100, 200))
	fmt.Println(NewItem(-100, 200))

	i.Move(700, 800)
	fmt.Printf("i (move): %#v\n", i)

	p1 := Player{
		Name: "Parzival",
		Item: Item{500, 600},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Println("p1.X:", p1.X)
	fmt.Println("p1.Item.X:", p1.Item.X)

	p1.Move(1, 2)
	fmt.Printf("p1 (move): %#v\n", p1)

	/* Before Key type
	fmt.Println(p1.Found("gold"))
	p1.Found("copper")
	p1.Found("copper")
	fmt.Println(p1.Keys) // [copper]
	*/
	fmt.Println(p1.Found(Key(20)))
	p1.Found(Copper)
	p1.Found(Copper)
	fmt.Println(p1.Keys) // [copper]

	ms := []Mover{
		&i,
		&p1,
	}
	moveAll(ms, 0, 0)
	for _, m := range ms {
		fmt.Println(m)
	}
}

/* Thought experiments
Go:
type Reader struct {
	Read([]byte) (int, error)
}

Python:
type Reader struct {
	Read(int) ([]byte, error)
}

type Sortable interface{
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
*/

/* What fmt.Print does

if s, ok := v.(fmt.Stringer); ok {
	// use s.String()
} else {
	switch v.(type) {
	case int:
		...
	}
}
*/

// implement fmt.Stringer
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Crystal:
		return "crystal"
	case Jade:
		return "jade"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Copper Key = iota + 1
	Crystal
	Jade
)

// Interfaces say what we need, not what we provide. The tend to be small.
// Rule of thumb: return types, accept interfaces
func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

type Mover interface {
	Move(int, int)
}

/*
func (p *Player) Move(x, y int) {
	log.Printf("info: %s moving to %d/%d", p.Name, x, y)
	p.Item.Move(x, y)
}
*/

/*
	Exercise:

- Add a Keys field to Player which is a []string
- Add a Found(key string) error method to Player that

  - Will err is key is not one of "jade", "copper", "crystal"

  - Will add key only once to Key

    p1.Found("copper")
    p1.Found("copper")
    fmt.Println(p1.Keys) // [copper]
*/
func (p *Player) Found(key Key) error {
	switch key {
	// case "copper", "jade", "crystal":
	case Copper, Jade, Crystal:
		// OK
	default:
		return fmt.Errorf("%v - unknown key", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}

	return nil
}

func (p *Player) hasKey(key Key) bool {
	for _, k := range p.Keys {
		if key == k {
			return true
		}
	}
	return false
}

type Player struct {
	Name string
	Keys []Key
	Item // Player embeds Item
	// Xer
}

/*
type Xer struct {
	X string
}
*/

// "i" is called "the receiver" (somewhat like this/self)
// Use a pointer receiver when changing values
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

/*
func NewItem(x, y int) Item
func NewItem(x, y int) *Item
func NewItem(x, y int) (Item, error)
*/
func NewItem(x, y int) (*Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d of of bounds for %d/%d", x, y, maxX, maxY)
	}

	i := Item{
		X: x,
		Y: y,
	}
	// Go does escape analysis and will allocate i on the heap
	return &i, nil
}

const (
	maxX = 1000
	maxY = 600
)

type Item struct {
	X int
	Y int
}
