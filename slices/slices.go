package main

import (
	"fmt"
	"sort"
)

func main() {
	var s1 []int
	fmt.Printf("s1: %#v type=%T, size=%d\n", s1, s1, len(s1))
	// nil slice "safe" operations: len, for range

	s2 := []int{0, 1, 2, 3, 5, 8}
	fmt.Printf("s2: %#v type=%T, size=%d\n", s2, s2, len(s2))

	s3 := s2[1:3] // slicing notation, half-empty range
	fmt.Printf("s3: %#v type=%T, size=%d\n", s3, s3, len(s3))
	fmt.Printf("s3: len=%d, cap=%d\n", len(s3), cap(s3))

	n := 3
	fmt.Println("slice:", s2[:n], s2[n:])

	s3[0] = 99
	fmt.Printf("s3: %#v\n", s3)
	fmt.Printf("s2: %#v\n", s2)

	s3 = append(s3, 100)
	fmt.Printf("s3: %#v\n", s3)
	fmt.Printf("s2: %#v\n", s2)

	var s4 []int
	for i := 0; i < 1000; i++ {
		s4 = appendInt(s4, i)
	}
	fmt.Println(len(s4), "<>", s4[:5])

	values := []float64{2, 1, 3}
	fmt.Println(median(values)) // 2 <nil>
	values = append(values, 4)
	fmt.Println(median(values)) // 2.5 <nil>
	fmt.Println(values)

	cart := []string{"lemons", "apples", "bananas"}
	// pointer(ish) semantics
	for i := range cart { // indices
		fmt.Println(i)
	}
	for i, v := range cart { // indices + values
		fmt.Println(i, v)
	}
	// value semantics
	for _, v := range cart { // values
		fmt.Println(v)
		v = "oranges" // won't affect cart
	}
	fmt.Println(cart)
	/*
		for i := range cart { // indices
			cart[i] = "oranges" // will affect cart
		}
		fmt.Println(cart)
	*/
	fmt.Println(concat([]string{"A", "B"}, []string{"C"})) // [A B C]
}

// exercise
func concat(s1, s2 []string) []string {
	s := make([]string, len(s1)+len(s2))
	copy(s, s1)
	copy(s[len(s1):], s2)
	return s
}

func median(vals []float64) (float64, error) {
	if len(vals) == 0 {
		return 0, fmt.Errorf("median of empty slice")
	}

	values := make([]float64, len(vals))
	copy(values, vals)

	sort.Float64s(values)
	i := len(values) / 2
	if len(values)%2 == 1 { // odd number of values
		return values[i], nil
	}

	m := (values[i-1] + values[i]) / 2
	return m, nil
}

func appendInt(s []int, v int) []int {
	i := len(s)
	if len(s) < cap(s) { // enough space in underlying array
		s = s[:len(s)+1]
	} else { // need to reallocate & copy
		size := 2 * (len(s) + 1)
		fmt.Println(len(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[i] = v
	return s
}
