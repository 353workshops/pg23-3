package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	/* pointer demo
	var r Reply
	addRepo(&r)
	fmt.Println(r)
	*/

	// get()
	// decode()
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	fmt.Println(githubInfo(ctx, "tebeka")) // Miki Tebeka 64 <nil>
	fmt.Println(githubInfo(ctx, "ardanlabs"))
}

// Exercise:
func githubInfo(ctx context.Context, login string) (string, int, error) {
	url := "https://api.github.com/users/" + login
	// url := fmt.Sprintf("https://api.github.com/users/%s", login)
	// Look into http.Escape
	// req, err := http.NewRequest(http.MethodGet, url, nil)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, err
	}
	if resp.StatusCode != http.StatusOK {
		return "", 0, fmt.Errorf("%q: bad status - %s", url, resp.Status)
	}

	return parseResponse(resp.Body)
}

func parseResponse(rdr io.Reader) (string, int, error) {
	var r Reply
	dec := json.NewDecoder(rdr)
	if err := dec.Decode(&r); err != nil {
		log.Fatalf("error: decode JSON - %s", err)
	}

	return r.Name, r.NumRepos, nil
}

func decode() {
	// Goland: github/reply.json
	// Go idiom: acquire resource, check for error, defer release
	file, err := os.Open("reply.json")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	var r Reply
	dec := json.NewDecoder(file)
	if err := dec.Decode(&r); err != nil {
		log.Fatalf("error: decode JSON - %s", err)
	}
	fmt.Println(r)

	/*
		var i int // initialized to zero value
		fmt.Println(i) // 0
	*/
}

type Reply struct {
	Name     string
	NumRepos int `json:"public_repos"`
}

func get() {
	url := "https://api.github.com/users/tebeka"
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	// req.Header.Set("Authorization", "Bearer s3cr3t")

	// resp, err := http.Get(url)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("error: bad status - %s", resp.Status)
	}

	fmt.Println("content-type", resp.Header.Get("Content-Type"))
	// fmt.Println("content-type", resp.Header["Content-Type"])
	// io.Copy(os.Stdout, resp.Body)
}

/* JSON <-> Go Types
true/false (boolean) <-> bool
string <-> string
null <-> nil
number <-> float64, float32, int, int8, int16, int32, int64, uint, uint8 ...
array <-> []T, []any ([]interface{})
object <-> struct, map[string]any
MIA: time.Time, comment

encoding/json API
JSON -> io.Reader -> Go: json.Decoder
Go -> io.Writer -> JSON: json.Encoder
JSON -> []byte -> Go: Unmarshal
Go -> []byte -> JSON: Marshal
*/

// *T: type pointer to T
// &v: Pass a pointer to v
func addRepo(r *Reply) {
	r.NumRepos++
	// r->NumRepos++ // C/C++
}
