package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

func TestHealth(t *testing.T) {
	logger, err := zap.NewDevelopment()
	require.NoError(t, err, "create logger")
	api := API{
		log: logger.Sugar(),
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "/health", nil)

	api.healthHandler(w, r)

	resp := w.Result()
	require.Equal(t, http.StatusOK, resp.StatusCode, "status")
}
