package main

import (
	"encoding/json"
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"github.com/ardanlabs/nlp"
	"github.com/ardanlabs/nlp/stemmer"
)

var (
	tokCalls = expvar.NewInt("tokenize.calls")
)

/* configuration: defaults < config file < environment < command line
Default: code
Config file: YAML, TOML
Environment: os.Getenv
Command line: flag

external:
- viper + cobra
- ardanlabs/conf

Always validate your configuration before starting the server!
Also do a health check before starting the server!
*/

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("error: can't create logger - %s", err)
	}
	api := API{
		log: logger.Sugar(),
	}

	mux := chi.NewRouter()
	mux.Get("/health", api.healthHandler)
	mux.Post("/tokenize", api.tokenizeHandler)
	mux.Get("/stem/{word}", api.stemHandler)
	mux.Handle("/debug/vars", expvar.Handler())
	// option 2:
	// http.Handle("/", mux)

	/* before chi
	http.HandleFunc("/health", healthHandler)
	http.HandleFunc("/tokenize", tokenizeHandler)
	*/
	/* stdlib router (mux) does simple matching
	- if path ends with / it's a prefix match (e.g. /a/ will match /a /a/b ...)
	- else it's an exact match
	*/

	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Fatalf("error: %s", err)
	}
}

type API struct {
	log *zap.SugaredLogger
}

func (a *API) stemHandler(w http.ResponseWriter, r *http.Request) {
	word := chi.URLParam(r, "word")
	if word == "" {
		http.Error(w, "missing word", http.StatusBadRequest)
		return
	}

	a.log.Infow("stemHandler", "word", word, "stem", "stem")
	stem := stemmer.Stem(word)
	fmt.Fprintln(w, stem)
}

func (a *API) tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	/* before chi
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/
	tokCalls.Add(1)
	// Step 1: Get, parse & validate data
	rdr := http.MaxBytesReader(w, r.Body, 1_000_000)
	data, err := io.ReadAll(rdr)
	if err != nil {
		http.Error(w, "can't read", http.StatusBadRequest)
		return
	}
	text := string(data)

	// Step 2: Work
	tokens := nlp.Tokenize(text)

	// Step 3: Encode and send data
	resp := map[string]any{
		"tokens": tokens,
	}
	w.Header().Set("content-type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		log.Printf("error: can't write - %s", err)
	}
}

/* Exercise:
Write a tokenizeHandler that will return tokens in JSON format

$ curl -d"Who's on first?" http://localhost:8080/tokenize
{"tokens": ["who", "on", "first"]}

Hints:
- io.ReadAll
- var data []byte, s := string(data)
- encoding/json
*/

func (a *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	/* before chi
	if r.Method != http.MethodGet {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/

	// TODO: Real health check (db connection ...)
	fmt.Fprintln(w, "OK")
}
