package nlp

import (
	"os"
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

/*
	Exercise:

Read test cases from testdata/tokenize_cases.yml instead of in-memory
testCases.

If there's a name in the test case, use it, otherwise use the text.

Remember: You need exported fields (upper case) in your struct.
For YAML use "gopkg.in/yaml.v3"
*/
type tokCase struct {
	Name   string
	Text   string
	Tokens []string
}

func (t tokCase) TestName() string {
	if t.Name != "" {
		return t.Name
	}
	return t.Text
}

func loadTokenizeCases(t *testing.T) []tokCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err)
	defer file.Close()

	var tc []tokCase
	err = yaml.NewDecoder(file).Decode(&tc)
	require.NoError(t, err, "decode YAML")
	return tc
}

func TestTokenizeTable(t *testing.T) {
	// setup: call a function
	// teardown: defer
	/*
		var testCases = []struct {
			text   string
			tokens []string
		}{
			{"", nil},
			{"What's on second", []string{"what", "s", "on", "second"}},
		}
	*/

	for _, tc := range loadTokenizeCases(t) {
		t.Run(tc.TestName(), func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			require.Equal(t, tc.Tokens, tokens)
			/* Before testify
			if !reflect.DeepEqual(tc.tokens, tokens) {
				t.Fatalf("expected: %#v, got %#v", tc.tokens, tokens)
			}
			*/
			// testTokenizeCase(t, tc.text, tc.tokens)
		})
	}
}

/*
func testTokenizeCase(t *testing.T, text string, expected []string) {
	// ...
}
*/

func TestTokenize(t *testing.T) {
	text := "Who's on first?"
	expected := []string{"who", "on", "first"}
	tokens := Tokenize(text)
	require.Equal(t, tokens, expected)
	/* Before testify
	if !reflect.DeepEqual(expected, tokens) {
		t.Fatalf("expected: %#v, got %#v", expected, tokens)
	}
	*/
}

func isCI() bool {
	// On Jenkins use "BUILD_NUMBER"
	return os.Getenv("CI") != ""
}

func TestLong(t *testing.T) {
	if !isCI() {
		t.Skip("not in CI")
	}
	t.Log("running")
}

func FuzzTokenize(f *testing.F) {
	f.Add("0")
	re := regexp.MustCompile(`[a-zA-Z]`)

	fn := func(t *testing.T, text string) {
		tokens := Tokenize(text)
		if re.MatchString(text) && len(tokens) == 0 {
			t.Fatalf("text: %q, tokens=%v", text, tokens)
		}
	}
	f.Fuzz(fn)
}
