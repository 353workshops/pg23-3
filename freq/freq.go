package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

var (
	// `` is a "raw" string

	// "Who's on first?" -> [Who s on first]
	wordRe = regexp.MustCompile(`[A-Za-z]+`)
)

// runs before main, considered a "code smell"
func init() {
	fmt.Println("<init>")
}

// What is the most common word (ignoring case) in shrelock.txt?
// - Go over file line by line: bufio.Scanner ✓
// - Split text to words: regular expression ✓
// - Count words: map ✓

func main() {
	file, err := os.Open("sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	freq := make(map[string]int) // word -> count
	s := bufio.NewScanner(file)
	for s.Scan() {
		for _, w := range wordRe.FindAllString(s.Text(), -1) {
			freq[strings.ToLower(w)]++
		}
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}

	maxW, maxC := "", 0
	for w, c := range freq {
		if c > maxC {
			maxW, maxC = w, c
		}
	}
	fmt.Println(maxW)

	// lineByLine()
	// splitToWords()
	// mapDemo()

}

/*
type Symbol string
type Price float64
*/

func mapDemo() {
	// var stocks map[Symbol]Price
	var stocks map[string]float64 // symbol -> price
	fmt.Printf("stocks: len=%d, type=%T\n", len(stocks), stocks)
	// len & for range are map nil safe
	aapl := stocks["AAPL"]
	// accessing a missing key will return zero value for type of values
	fmt.Println("AAPL:", aapl)

	if price, ok := stocks["AAPL"]; ok {
		fmt.Println("AAPL:", price)
	} else {
		fmt.Println("AAPL not found")
	}

	// stocks["AAPL"] = 148.23 // will panic on uninitialized map

	stocks = make(map[string]float64)
	stocks["AAPL"] = 148.23 // will panic on uninitialized map
	fmt.Println("AAPL:", stocks["AAPL"])

	stocks = map[string]float64{
		"AAPL":   148.23,
		"BRKA-A": 462_663.10,
	}
	fmt.Println(stocks)

	// for range as in slices, replace index with key
}

func splitToWords() {
	text := "Who's on first?"
	matches := wordRe.FindAllString(text, -1)
	fmt.Println(matches)
}

func lineByLine() {
	file, err := os.Open("sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	count := 0
	for s.Scan() {
		// s.Text()
		count++
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(count)
}
