package main

import (
	"sync"
	"sync/atomic"
	"testing"
)

var (
	mu      sync.Mutex
	counter int64
)

func BenchmarkMutex(b *testing.B) {
	for i := 0; i < b.N; i++ {
		/*
			mu.Lock()
			counter++
			mu.Unlock()
		*/
		incMutex()
	}
}

func incMutex() {
	mu.Lock()
	defer mu.Unlock()

	counter++
}

func BenchmarkAtomic(b *testing.B) {
	for i := 0; i < b.N; i++ {
		incAtomic()
	}
}

func incAtomic() {
	atomic.AddInt64(&counter, 1)
}
