package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	/* Solution 1: Use a mutex
	var mu sync.Mutex
	count := 0
	*/
	count := int64(0)

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for i := 0; i < 1000; i++ {
				time.Sleep(time.Microsecond)
				// Solution 2: Use sync/atomic
				atomic.AddInt64(&count, 1)

				/* Solution 1
				mu.Lock()
				count++
				mu.Unlock()
				*/
			}
		}()
	}
	wg.Wait()

	fmt.Println("count:", count)
}
