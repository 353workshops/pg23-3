package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
)

func main() {
	err := killServer("app.pid")
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			fmt.Println("missing file")
		}
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Println(">", e)
		}
		log.Fatalf("error: %s", err)
	}
}

func killServer(pidFile string) error {
	// open file
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	// defer file.Close()
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("warning: can't close %q - %s", pidFile, err)
		}
	}()

	// convert to int
	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		// %w will chain errors
		return fmt.Errorf("%q: bad pid - %w", pidFile, err)
	}

	// issue kill (send signal)
	fmt.Printf("killing %d\n", pid) // simulate kill
	if err := os.Remove(pidFile); err != nil {
		log.Printf("warning: can't delete %q - %s", pidFile, err)
	}
	return nil
}
