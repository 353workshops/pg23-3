package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 3; i++ {
		/* Fix 2: Use local loop variable */
		i := i // will "shadow" i from line 12
		go func() {
			fmt.Println(i)
		}()

		/* Fix 1: Use a parameter
		go func(n int) {
			fmt.Println(n)
		}(i)
		*/

		/*
			// BUG: All goroutines "see" the same i
			go func() {
				fmt.Println(i)
			}()
		*/
	}

	// CPA: Communicating sequential processes
	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	/*
		g := Greeter{"Bob"}
		go g.Greet("Alice")
	*/
	time.Sleep(10 * time.Millisecond)

	fmt.Println(sleepSort([]int{30, 10, 20})) // [10 20 30]

	go func() {
		defer close(ch)
		for i := 0; i < 3; i++ {
			ch <- i
		}
	}()

	for n := range ch {
		fmt.Println("range:", n)
	}

	/* What "for range" does
	for {
		n, ok := <- ch
		if !ok {
			break
		}
		fmt.Println("range:", n)
	}
	*/

	n := <-ch // receive from a closed channel
	fmt.Println("closed:", n)
	n, ok := <-ch
	fmt.Println("closed:", n, ok)
	// ch <- 7 // send to a closed channel - panic
	// close(ch) // closing a closed channel - panic

	// var ch1 chan int // nil channel

	ch1, ch2 := make(chan string), make(chan string)
	go func() {
		time.Sleep(50 * time.Millisecond)
		ch1 <- "one"
	}()
	go func() {
		time.Sleep(100 * time.Millisecond)
		ch2 <- "two"
	}()

	/*
		done := make(chan bool)
		go func() {
			time.Sleep(10 * time.Millisecond)
			close(done)
		}()
	*/

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Println("got", val, "from ch1")
	case val := <-ch2:
		fmt.Println("got", val, "from ch2")
		/*
			case <-time.After(10 * time.Microsecond):
				fmt.Println("timeout")
		*/
		/*
			case <-done:
				fmt.Println("cancelled")
		*/
	case <-ctx.Done():
		fmt.Println("context expired")
	}
}

/*
Channel semantics
- send/receive will block until opposite operation (*)
	- channel with buffer "n" has "n" non-block sends
- receive from a closed channel will return zero value without blocking
	- use "n, ok := <- ch" to find value is from closed channel
- send to a closed channel will panic
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
*/

/*
type Greeter struct {
	Name string
}

func (g *Greeter) Greet(name string) {
	fmt.Printf("Hi %s, I'm %s.\n", name, g.Name)
}
*/

/*
for every value "n" in values, spin a goroutine that will
  - sleep n milliseconds
  - send n over a channel

collect all values from the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)

	// fan out
	for _, n := range values {
		n := n // avoid closure bug
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var sorted []int
	for range values {
		n := <-ch
		sorted = append(sorted, n)
	}

	return sorted
}
