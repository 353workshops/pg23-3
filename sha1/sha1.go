package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fmt.Println(fileSHA1("http.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))
	// GoLand
	// fmt.Println(fileSHA1("sha1/http.log.gz"))
}

// cat http.log.gz| gunzip | sha1sum
// cat sha1.go | sha1sum
// Exercise: only decompress the file is it ends with ".gz"
// Hint: strings.HasSuffix
func fileSHA1(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		r, err = gzip.NewReader(file)
		// r, err := gzip.NewReader(file) // BUG
		if err != nil {
			return "", err
		}
	}

	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
