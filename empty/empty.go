package main

import "fmt"

func main() {
	// var i interface{} // go < 1.18
	var i any

	i = 7
	fmt.Println("i:", i)

	i = "Hi"
	fmt.Println("i:", i)

	// rule of thumb: don't use any

	s := i.(string) // type assertion
	fmt.Println("s:", s)

	// n := i.(int) // will panic
	n, ok := i.(int)
	if !ok {
		fmt.Println("not an int")
	} else {
		fmt.Println("n:", n)
	}
}
