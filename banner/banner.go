package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	// fmt.Println(9 / 2)
	banner("Go", 6)
	//	banner("ArdanLabs", 6)
	banner("G☻", 6)

	// string: array of bytes or array or runes? (chars/code points)
	// len() returns bytes
	// str[i] return byte in index i
	// for loop will go over runes (chars/code points)

	text := "Go ♡ You"
	// text[0] = 'g' // strings are immutable
	for i := range text {
		fmt.Println(i)
	}
	for i, c := range text {
		// fmt.Println(i, c)
		fmt.Printf("%d: %c\n", i, c)
		// %d is called "a verb"
	}
}

// banner("Go", 6)
//
//	  Go
//	------
func banner(text string, width int) {
	// FIXME: check width >= len(text)
	// padding := (width - len(text)) / 2
	padding := (width - utf8.RuneCountInString(text)) / 2
	// padding := float64(width) / 2.0
	for i := 0; i < padding; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)

	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}

// Go numbers: int, int8, int16, int32, int64
//             uint, uint8, uint16, uint32, uint64
//             float32, float64
