package main

import (
	"fmt"
	"log"
	"net"
	"strings"
	"sync"
	"time"
)

func main() {
	p := Payment{
		From:   "Wile E. Coyote",
		To:     "ACME",
		Amount: 123.45,
	}
	p.Process()
	p.Process()

	hosts := []string{
		"apple.com",
		"ibm.com",
		"ua.com",
	}

	var wg sync.WaitGroup
	wg.Add(len(hosts))
	for _, host := range hosts {
		// wg.Add(1)
		/*
			go func(h string) {
				defer wg.Done()
				addrs, err := net.LookupHost(h)
				if err != nil {
					log.Printf("error: %q - %s", h, err)
					return
				}
				log.Printf("info: %q - %s", h, strings.Join(addrs, ", "))
			}(host)
		*/
		go lookup(host, &wg)
	}
	wg.Wait() // will block until all work is done
}

func lookup(host string, wg *sync.WaitGroup) {
	defer wg.Done()
	addrs, err := net.LookupHost(host)
	if err != nil {
		log.Printf("error: %q - %s", host, err)
		return
	}
	log.Printf("info: %q - %s", host, strings.Join(addrs, ", "))
}

func (p *Payment) Process() {
	p.once.Do(func() {
		p.process(time.Now().UTC())
	})
}

func (p *Payment) process(t time.Time) {
	ts := t.Format(time.RFC3339)
	fmt.Printf("[%s] %s -> $%.2f -> %s\n", ts, p.From, p.Amount, p.To)
}

type Payment struct {
	From   string
	To     string
	Amount float32 // USD

	once sync.Once
}
