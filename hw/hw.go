package main

// single line comment
/*
multi line
comment
*/

// import "fmt"
import (
	"fmt"
)

func main() {
	fmt.Println("Hello Gophers ♡")
}
